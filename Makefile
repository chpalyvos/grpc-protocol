CXX = g++
CPPFLAGS += `pkg-config --cflags protobuf grpc`
CXXFLAGS += -std=c++11
LDFLAGS += -L/usr/local/lib `pkg-config --libs protobuf grpc++` \
           -lgrpc++_reflection \
           -ldl \
	   -Winconsistent-missing-override
PROTOC = protoc
GRPC_CPP_PLUGIN = grpc_cpp_plugin
GRPC_CPP_PLUGIN_PATH ?= `which $(GRPC_CPP_PLUGIN)`

.PHONY: clean all

all: server client

server: master.pb.o master.grpc.pb.o server.o
	$(CXX) $^ $(LDFLAGS) -o $@

client: master.pb.o master.grpc.pb.o client.o
	$(CXX) $^ $(LDFLAGS) -o $@

%.grpc.pb.cc: %.proto
	$(PROTOC) --grpc_out=. --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN_PATH) $<

%.pb.cc: %.proto
	$(PROTOC) --cpp_out=. $<

clean:
	rm -f *.in *.out *.o *.pb.cc *.pb.h server client

