// See tutorial at https://grpc.io/docs/tutorials/basic/c.html

#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <memory>
#include <string>
#include <fstream>
#include <stdlib.h>


#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>
#include "master.grpc.pb.h"

#define MAX_CHUNK_SIZE 1024

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;
using master::Master;
using master::VariableName;
using master::VariableIntegerValue;
using master::VariableStringValue;
using master::Chunk;
using master::FileInfo;
using master::Result;

std::string upload_filename;
std::string _string;
int _int;


class ServerNode final : public Master::Service {
	private:
		std::fstream out_file;
	public:
		Status getInteger(ServerContext* context, const VariableName* var_int,
							VariableIntegerValue* val_int){

			std::cout << "Server: request to read integer variable " << var_int->name() <<std::endl;

			val_int->set_value(42);

			return Status::OK;
		}
		Status getString(ServerContext* context, const VariableName* var_str,
							VariableStringValue* val_str){

			std::cout << "Server: request to read string variable " << var_str->name() <<std::endl;

			val_str->set_value("A string");

			return Status::OK;
		}
		
		// "server-side streaming"
		// The client receives a stream of data from the server.		
		Status traceInteger(ServerContext* context, const VariableName* var_int,
								ServerWriter<VariableIntegerValue>* writer){

			std::cout << "Server: request to trace integer variable: " << var_int->name() << std::endl;

			VariableIntegerValue val_int;

			val_int.set_value(_int);

			writer->Write(val_int);

			return Status::OK;
		}

		Status traceString(ServerContext* context, const VariableName* var_str,
								ServerWriter<VariableStringValue>* writer){

			std::cout << "Server: request to trace string variable: " << var_str->name() << std::endl;

			VariableStringValue val_str;

			val_str.set_value(_string);

			writer->Write(val_str);

			return Status::OK;
		}

     	Status setFileName(ServerContext* context, const FileInfo* file,
           			       	Result* res) override {
			std::cout << "Server: request to upload data on file : " << file->filename() << std::endl;
			upload_filename = file->filename();
            this->out_file = std::fstream(upload_filename, std::ios::out | std::ios::binary | std::ios::trunc);
            this->out_file.seekp(std::ios::beg);
  			return Status::OK;
		}
        

        // "client-side streaming"
        // The client sends a stream of data to the server.
		Status uploadFile(ServerContext* context, const Chunk* chunk,
                     		Result* result) override {
			this->out_file.write( chunk->data().c_str(), chunk->size());
			return Status::OK;
		}

		Status closeFile(ServerContext* context, const FileInfo* file,
                  		Result* res){
			this->out_file.close();
			return Status::OK;
		}
		
};


int main (int argc, const char *argv[]){

	// Argument parsing
	if (argc < 3){
		std::cout << "Please execute the command as follows:"
		<< std::endl << "./server integer string"
		<< std::endl << "e.g. ./server 34 my string is that"
		<< std::endl;
		exit(-1);
	}
	// files to write integer and string on "server side" for testing purposes
	std::ofstream integer_f, string_f;
	integer_f.open("integer.in", std::ios::out | std::ios::trunc);
	string_f.open("string.in", std::ios::out | std::ios::trunc);

	_int = std::atoi(argv[1]);

	std::string s1;
	std::string s2 = " ";

	for (int i = 2; i < argc; i++){
		s1 = argv[i];
		_string.append(s1);
		_string.append(s2); 
	}
	_string.erase(_string.end()-1);

	integer_f << _int;
	string_f << _string;

	integer_f.close();
	string_f.close();

	std::string server_address("0.0.0.0:50051");
  	ServerNode service;
  	ServerBuilder builder;
  	builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  	builder.RegisterService(&service);
  	std::unique_ptr<Server> server(builder.BuildAndStart());
  	std::cout << "Server: listening on " << server_address << std::endl;
  	server->Wait();
	return 0;
}
