#define MAX_CHUNK_SIZE 16*1024 // 16KB after much search

#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <memory>
#include <string>
#include <fstream>
#include <thread>
#include <vector>
#include <stdlib.h>
#include <string.h>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include "master.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using grpc::Status;
using master::Master;
using master::VariableName;
using master::VariableIntegerValue;
using master::VariableStringValue;
using master::Chunk;
using master::FileInfo;
using master::Result;


class ClientNode {

		std::unique_ptr<Master::Stub> stub_;
		std::shared_ptr<Channel> channel_;

	public:

		ClientNode(std::string server_address) {

			channel_ = grpc::CreateChannel(server_address, grpc::InsecureChannelCredentials());

			stub_ = Master::NewStub(channel_);
		}

		bool getInteger(const VariableName* name_int, VariableIntegerValue* val_int){
			
			ClientContext *context = new ClientContext;

			Status status = stub_->getInteger(context, *name_int, val_int);

			if (! (status.ok())) {
				return false;
			}
			return true;
		}

		bool getString(const VariableName* name_str, VariableStringValue* val_str){
			
			ClientContext *context = new ClientContext;

			Status status = stub_->getString(context, *name_str, val_str);

			if (! (status.ok())) {
				return false;
			}
			return true;
		}

		// "server-side streaming"
		// The client receives a stream of data from the server.
  		bool traceInteger(const VariableName* name_int){
  			
  			ClientContext context;

  			VariableIntegerValue val_int;

  			std::unique_ptr<ClientReader<VariableIntegerValue> > reader(
  							stub_->traceInteger(&context, *name_int));
  			
  			while(reader->Read(&val_int)){
  				std::cout << "Client: integer received value: " << val_int.value() <<std::endl;
  			}

  			Status status = reader->Finish();

  			if (! (status.ok())){
  				return false;
  			}
  			// file to write integer on "client side" for testing purposes
  			std::ofstream int_file;
  			int_file.open("integer.out", std::ios::out);
  			int_file << val_int.value();
  			int_file.close();
  			return true;
  		}

  		bool traceString(const VariableName* name_str){
  			
  			ClientContext context;

  			VariableStringValue val_str;

  			std::unique_ptr<ClientReader<VariableStringValue> > reader(
  							stub_->traceString(&context, *name_str));
  			
  			while(reader->Read(&val_str)){
  				std::cout << "Client: received string value: " << val_str.value() <<std::endl;
  			}

  			Status status = reader->Finish();

  			if (! (status.ok())){
  				return false;
  			}
   			// file to write string on "client side" for testing purposes
  			std::ofstream str_file;
  			str_file.open("string.out", std::ios::out);
  			str_file << val_str.value();
  			str_file.close();
  			return true;
  		}

		// "client-side streaming"
		// The client sends a stream of data to the server.
		bool uploadFile(const std::string local_filename, const std::string remote_filename){
			
			std::ifstream::pos_type total_size;
			char * memory_block;
			std::ifstream file;
			FileInfo server_file;
			Result reply;
			Chunk chunk;
			ClientContext *context2;
	    	ClientContext context;

			file.open(local_filename, std::ios::in | std::ios::binary | std::ios::ate);
			
			if (file.is_open()){

				int current_block_size = 0;
				// find file size
				file.seekg(0, std::ios::end);
				std::ifstream::pos_type end_size = file.tellg();
				file.seekg(0, std::ios::beg);
				std::ifstream::pos_type beg_size = file.tellg();
				total_size = end_size - beg_size;

				std::cout << "Client: Total file size: " << total_size << std::endl;

				server_file.set_filename(remote_filename);

				Status status = stub_->setFileName(&context, server_file, &reply);
				
				// split into chunks and upload each chunk
				// if file is smaller than MAX_CHUNK_SIZE upload it directly
				if (total_size < MAX_CHUNK_SIZE){
					// dynamically allocate and deallocate context2 
					//as we write several times on it
					context2 = new ClientContext;
					// memory block (buffer) for the bytes to be sent
					memory_block = new char[total_size];
					// read this block
					file.read(memory_block, total_size);
					//set chunk fields
					chunk.set_data(memory_block);
					// free the block for better memory management
					delete[] memory_block;
					memory_block = NULL;
					chunk.set_size(total_size);
					// upload the chunk to remote server
					Status status = stub_->uploadFile(context2, chunk, &reply);
				}
				else{
					for (int i = 0; i < total_size; i+=MAX_CHUNK_SIZE){
						
						// final chunk might not have maximum chunk size
						// check if so
						if (i + MAX_CHUNK_SIZE > total_size)
							current_block_size = total_size % MAX_CHUNK_SIZE;
						else
							current_block_size = MAX_CHUNK_SIZE;
						
						context2 = new ClientContext;
						memory_block = new char[current_block_size];

						//std::cout << "Client: read " << current_block_size << "bytes" << std::endl;

						file.read(memory_block, current_block_size);

						chunk.set_data(memory_block);
						delete[] memory_block;
						memory_block = NULL;

		    			chunk.set_size(current_block_size);

		    			Status status = stub_->uploadFile(context2, chunk, &reply);

		    			delete context2;
		    			context2 = NULL;

		    			if (file.eof()) 
		    				break;
					}
				}
			}
			else{
				exit(-4);
			}
			ClientContext context3;
			Status status = stub_->closeFile(&context3, server_file, &reply);
			file.close();
    		return true;
		}
};


int main (int argc, const char *argv[]){

	// Argument parsing
	std::vector< std::pair<std::string, std::string> > arguments;

	if (argc < 2 || (argc - 1)%2==1) {
		std::cout << "Please execute the command ad follows:\n ./client path_to_input_file path_to_output_file [path_to_input_file path_to_output_file]" 
				<< std::endl 
				<< "which means : ./client {path_to_input_file path_to_output_file}+" 
				<<std::endl
				<< "at least one, but as many pairs of input/output files you wish"
				<<std::endl;
				exit(-1);
	}

	// files to write the input (client side) and output (server side) files for testing purposes
	std::ofstream files_in, files_out;
	files_in.open("filenames.in", std::ios::out | std::ios::trunc);
	files_out.open("filenames.out", std::ios::out | std::ios::trunc);

	for (int i = 1; i < argc; i+=2){
		arguments.push_back(std::make_pair(argv[i], argv[i+1]));
		files_in << argv[i];
		files_in << "\n";
		files_out << argv[i+1];
		files_out << "\n";
	}

	files_in.close();
	files_out.close();

	std::string server_address ("localhost:50051");
	ClientNode client(server_address);

	//////////////// INTEGER
	
	VariableName name_integer;

	VariableIntegerValue ret_int;

	name_integer.set_name("number_to_server");

	name_integer.set_name("number_from_server");

	client.traceInteger(&name_integer);

	////////////////// STRING

	VariableName name_string;

	VariableStringValue ret_str;

	name_string.set_name("string_to_server");

	name_string.set_name("string_from_server");

	client.traceString(&name_string);

	///////////////// FILES

	std::vector< std::pair<std::string, std::string> >::iterator it;
	for (it = arguments.begin(); it != arguments.end(); ++it){
		client.uploadFile(it->first, it->second);
	}

	return 0;
}
