#!/usr/bin/env python3

# function to test if 2 files are the same
def are_same(file_1, file_2):
	try:	
		with open(file_1, 'r') as fl_1:
			with open(file_2, 'r') as fl_2:
				for line1,line2 in zip(fl_1, fl_2):
					if  (line1 != line2):
						return False
	except UnicodeDecodeError: # possibly on binary files
		import subprocess
		print("Unicode Decode exception raised.")
		# use of diff bash command
		bash_command = "diff " + file_1 +" " + file_2 
		output = subprocess.run(bash_command.split(), stdout=subprocess.PIPE)
		if "differ" in output.stdout.decode('utf-8'):
			return False
		pass
	return True

## TEST INTEGER
with open("integer.in", 'r') as integer_in:
	with open("integer.out", 'r') as integer_out:
		int_in = integer_in.readlines()[0].strip()
		int_out = integer_out.readlines()[0].strip()
		if int_in != int_out:
			print("Integer TEST: FAIL. (integer_in should be same as integer_out)")
		else:
			print ("Integer TEST: PASS")

## TEST STRING
with open("string.in", 'r') as string_in:
	with open("string.out", 'r') as string_out:
		str_in = string_in.readlines()[0].strip()
		str_out = string_out.readlines()[0].strip()
		if str_in != str_out:
			print("String TEST: FAIL. (string_in should be same as string_out)")
		else:
			print ("String TEST: PASS")

## TEST FILE
with open("filenames.in", 'r') as fl_in:
	with open("filenames.out", 'r') as fl_out:
		files_in_list = fl_in.readlines()
		files_in_list = [f.strip() for f in files_in_list]
		files_out_list = fl_out.readlines()
		files_out_list = [f.strip() for f in files_out_list]

		for f1,f2 in zip(files_in_list,files_out_list):
			if (are_same(f1, f2)):
				print("File {} TEST: PASS".format(f1))
			else:
				print("File {} TEST: FAILED. It's not the same with file {}".format(f1, f2))